<?php

namespace FrontBundle\Controller;

use CoreBundle\Controller\Controller;

use Facebook\FacebookRequest;
use Symfony\Component\HttpFoundation\Request;

class QuizzController extends Controller
{
    public function indexAction(Request $request)
    {
        $fb = new \Facebook\Facebook([
            'app_id' => $this->container->getParameter('oauth.facebook.id'),
            'app_secret' => $this->container->getParameter('oauth.facebook.secret'),
            'default_graph_version' => 'v2.5',
        ]);


        $helper = $fb->getRedirectLoginHelper();

        $rep = $request->query->get('code');
        if($rep == null){

            $permissions = ['email','user_friends'];
            $callback    = 'http://daily-quizz.local/web/app_dev.php/quizz/';
            $loginUrl    = $helper->getLoginUrl($callback, $permissions);
            return $this->redirect($loginUrl);

        } else {


            $accessToken = $helper->getAccessToken();


            try {
                $response = $fb->get('/me/taggable_friends?fields=picture.type(large)&limit=1000', $accessToken);


                var_dump($response->getGraphEdge());
               $friends =array();
                foreach($response->getDecodedBody() as $key => $data ){
                   var_dump($key);
                    if($key == 'data'){
                        foreach($data as $value){
                            $friends[] = $value['picture']['data']['url'];
                        }

                    } elseif($key == 'paging'){

                    }

                }

                var_dump($friends);
                die();
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
        }



        return $this->render('FrontBundle:Quizz:index.html.twig', array(

        ));
    }




}
