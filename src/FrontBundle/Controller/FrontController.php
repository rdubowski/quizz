<?php

namespace FrontBundle\Controller;

use CoreBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{
    public function indexAction(Request $request)
    {


        return $this->render('FrontBundle::index.html.twig', array(

        ));
    }


}
