<?php

namespace FrontBundle\Controller;

use CoreBundle\Controller\Controller;

use Facebook\FacebookRequest;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\Request;

class CombienTempsCoupleController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('FrontBundle:CombienTempsCouple:index.html.twig', array(

        ));
    }

    public function quizzAction(Request $request){
        $fb = new \Facebook\Facebook([
            'app_id' => $this->container->getParameter('oauth.facebook.id'),
            'app_secret' => $this->container->getParameter('oauth.facebook.secret'),
            'default_graph_version' => 'v2.5',
        ]);



        $helper = $fb->getRedirectLoginHelper();



        $rep = $request->query->get('code');
        if($rep == null){

            $permissions = ['email'];
            $callback    = $this->container->getParameter('domain').'/combien-de-temps-de-vie-en-couple/quizz';
            $loginUrl    = $helper->getLoginUrl($callback, $permissions);
            return $this->redirect($loginUrl);

        } else {


            $accessToken = $helper->getAccessToken();


            try {
                $response = $fb->get('/me?fields=id,first_name,email,picture.type(large)', $accessToken);

                $image = new ImageManager(array('driver' => 'gd'));

                $body =$response->getDecodedBody();

                    $image->make($body['picture']['data']['url'])
                        ->fit(170, 170)
                        ->rotate(10)

                        ->save( 'facebook/'.$body['id'].'.png',100);
                    $img = $this->container->getParameter('domainImage').'/facebook/'.$body['id'].'.png';

                $annee = rand(30,60);

                return $this->render('FrontBundle:CombienTempsCouple:image.html.twig', array(
                    'domain' => $this->container->getParameter('domainImage'),
               // $html = $this->renderView('FrontBundle:CombienTempsCouple:image.html.twig', array(
                    'img' => $img,
                    'annee' => $annee,
                    'prenom' => $body['first_name'],
                ));

                $this->get('knp_snappy.image')->generateFromHtml(
                    $html,
                    'passeport/image/projet-'.$projet->getId().'.jpg'
                );
                $friends =array();
                foreach($response->getDecodedBody() as $key => $data ){
                    var_dump($key);
                    if($key == 'data'){
                        foreach($data as $value){
                            $friends[] = $value['picture']['data']['url'];
                        }

                    } elseif($key == 'paging'){

                    }

                }

                var_dump($friends);
                die();
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
        }



        return $this->render('FrontBundle:Quizz:index.html.twig', array(

        ));
    }

    public function reponseAction(Request $request)
    {

        return $this->render('FrontBundle:CombienTempsCouple:index.html.twig', array(

        ));
    }



}
