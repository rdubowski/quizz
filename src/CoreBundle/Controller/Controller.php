<?php

namespace CoreBundle\Controller;

use CoreBundle\Doctrine\Repository\BanqueRepository;
use CoreBundle\Doctrine\Repository\CommuneRepository;
use CoreBundle\Doctrine\Repository\ActualiteRepository;
use CoreBundle\Doctrine\Repository\EmailRepository;
use CoreBundle\Doctrine\Repository\VillesRepository;
use CoreBundle\Doctrine\Repository\RegionRepository;
use CoreBundle\Doctrine\Repository\DepartementRepository;
use CoreBundle\Doctrine\Repository\AvancementRepository;
use CoreBundle\Doctrine\Repository\CarteIdentiteRepository;
use CoreBundle\Doctrine\Repository\ContratRepository;
use CoreBundle\Doctrine\Repository\DonRepository;
use CoreBundle\Doctrine\Repository\JustifCarteIdentiteRepository;
use CoreBundle\Doctrine\Repository\JustifCompteRepository;
use CoreBundle\Doctrine\Repository\JustifContratRepository;
use CoreBundle\Doctrine\Repository\JustifElementRepository;
use CoreBundle\Doctrine\Repository\JustificatifRepository;
use CoreBundle\Doctrine\Repository\JustifImpotRepository;
use CoreBundle\Doctrine\Repository\JustifLoyerRepository;
use CoreBundle\Doctrine\Repository\JustifRibRepository;
use CoreBundle\Doctrine\Repository\JustifSalaireRepository;
use CoreBundle\Doctrine\Repository\MetierRepository;
use CoreBundle\Doctrine\Repository\NatureRepository;
use CoreBundle\Doctrine\Repository\ProfessionRepository;
use CoreBundle\Doctrine\Repository\ProfilCoEmprunteurRepository;
use CoreBundle\Doctrine\Repository\ProfilEmprunteurRepository;
use CoreBundle\Doctrine\Repository\ProjetRepository;
use CoreBundle\Doctrine\Repository\RelationRepository;
use CoreBundle\Doctrine\Repository\RubriqueRepository;
use CoreBundle\Doctrine\Repository\SalaireRepository;
use CoreBundle\Doctrine\Repository\SituationMaritaleRepository;
use CoreBundle\Doctrine\Repository\TemoignageRepository;
use CoreBundle\Doctrine\Repository\TypeBienRepository;
use CoreBundle\Doctrine\Repository\UserProRepository;
use CoreBundle\Doctrine\Repository\UserRepository;
use CoreBundle\Entity\CarteIdentite;
use CoreBundle\Entity\User;
use CoreBundle\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use CoreBundle\Entity\CodePostal;

abstract class Controller extends BaseController
{
    /**
     * @return CsrfProviderInterface
     */
    protected function getCsrfProvider()
    {
        return $this->get('form.csrf_provider');
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     * @return SecurityContextInterface
     */
    protected function getSecurityContext()
    {
        return $this->container->get('security.context');
    }





}
