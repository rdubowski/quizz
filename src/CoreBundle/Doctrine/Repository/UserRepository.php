<?php

namespace CoreBundle\Doctrine\Repository;

use CoreBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 */
class UserRepository extends EntityRepository
{
    public function getAllUsersQueryBuilder()
    {
        $qb = $this->createQueryBuilder('u');
        $expr = $qb->expr();

        $qb
            ->orderBy('u.lastName')
            ->where($expr->notLike('u.roles', '\'%' . User::ROLE_ADMIN . '%\'')) // Not fetching ADMIN
            ->where($expr->notLike('u.roles', '\'%' . User::ROLE_SUPER_ADMIN . '%\'')) // Not fetching SUPER_ADMIN
        ;

        return $qb;
    }
}
