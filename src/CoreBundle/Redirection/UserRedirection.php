<?php
namespace CoreBundle\Redirection;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserRedirection implements AuthenticationSuccessHandlerInterface
{
    private $router;

    public function __construct(RouterInterface $router){
        $this->router = $router;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token){
        $rolesTab = $token->getRoles();
        $redirection = new RedirectResponse($this->router->generate('front_compte_index'));

        foreach($rolesTab as $role){
            if($role->getRole() == "ROLE_PRO"){
                $redirection = new RedirectResponse($this->router->generate('front_compte_pro_index'));
            }
            if($role->getRole() == "ROLE_ADMIN"){
                $redirection = new RedirectResponse($this->router->generate('admin_homepage'));
            }
        }

        return $redirection;
    }
}
