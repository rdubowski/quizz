<?php

namespace CoreBundle\Util;

class SlugHandler
{
    public function generate($text)
    {
    	// Fix fonction de bouzeu
    	$text = htmlentities($text, ENT_NOQUOTES, 'utf-8');
    	$text = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $text);
    	$text = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $text); // pour les ligatures e.g. '&oelig;'
    	$text = preg_replace('#&[^;]+;#', '', $text); // supprime les autres caractères
    	
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
