<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 15/10/2015
 * Time: 09:29
 */

namespace CoreBundle\Util;


class AdresseFromCoords
{
    function getAdresseFromCoords($lat, $lng)
    {
        $content = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&sensor=false');

        if(!$content)
            return false;

        $json = json_decode($content, true);

        if(!$json || $json['status'] != 'OK')
            return false;

        if(!isset($json['results'][0]['formatted_address']) && empty($json['results'][0]['formatted_address']))
            return false;

        return $json['results'][0]['formatted_address'];
    }
}
