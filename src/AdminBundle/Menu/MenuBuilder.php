<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class MenuBuilder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav nav-tabs nav-stacked main-menu');

        $menu->addChild('dashboard', array(
            'route' => 'admin_homepage',
            'label' => 'Accueil',
            'extras' => array(
                'icon' => 'fa fa-bar-chart-o',
            )
        ));

        return $menu;
    }
}
