/*global window */
(function (window, $, document) {
    'use strict';

    var initialize = function (context) {


        var fileInputVousCo = $('#projet_etape_3_co_projet_form_type_user_photoProfilPath');

        fileInputVousCo.on('change', function () {
            var lastItem = fileInputVousCo.val().split("\\").pop(-1);
            lastItem = lastItem.substring(0, 20);
            $(".txtVous").html(lastItem + '...');
        });

        var fileInputProjetCo = $('#projet_etape_3_co_projet_form_type_photoProjetPath');

        fileInputProjetCo.on('change', function () {
            var lastItem = fileInputProjetCo.val().split("\\").pop(-1);
            lastItem = lastItem.substring(0, 20);
            $(".txtProjet").html(lastItem + '...');
        });

        var fileInputDon = $('#temoignage_don_form_type_user_photoProfilPath');

        fileInputDon.on('change', function () {
            var lastItem = fileInputDon.val().split("\\").pop(-1);
            lastItem = lastItem.substring(0, 20);
            $(".txtDon").html(lastItem + '...');
        });



        var fileInputVous = $('#projet_etape_3_projet_form_type_user_photoProfilPath');

        fileInputVous.on('change', function () {
            var lastItem = fileInputVous.val().split("\\").pop(-1);
            lastItem = lastItem.substring(0, 20);
            $(".txtVous").html(lastItem + '...');
        });

        var fileInputProjet = $('#projet_etape_3_projet_form_type_photoProjetPath');

        fileInputProjet.on('change', function () {
            var lastItem = fileInputProjet.val().split("\\").pop(-1);
            lastItem = lastItem.substring(0, 20);
            $(".txtProjet").html(lastItem + '...');
        });

        var fileInputProfil = $('#registration_profil_photoProfilPath');

        fileInputProfil.on('change', function () {
            var lastItem = fileInputProfil.val().split("\\").pop(-1);
            lastItem = lastItem.substring(0, 35);
            $(".txtProfil").html(lastItem + '...');
        });



       /* $('#sandbox-container input').datepicker({
            format: "dd/mm/yyyy",
            language: "fr",
            showDropdowns: true
        });*/

    };

    $(document).on('bw_load', function (event) {
        initialize(event.currentTarget);
    });

    $(function () {
        initialize(document);
    });
})(window, window.jQuery, window.document);
