/*global window */
(function (window, $, document) {
    'use strict';

    var initialize = function (context) {
        // Bootstrap Tooltip
        $('[data-toggle="tooltip"]', context).tooltip();
    };

    $(document).on('bw_load', function (event) {
        initialize(event.currentTarget);
    });

    $(function () {
        initialize(document);
    });
})(window, window.jQuery, window.document);
