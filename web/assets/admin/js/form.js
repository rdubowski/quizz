/*global window */
(function (window, $, document) {
    'use strict';

    var initialize = function (context) {
        // Chosen
        $('[data-widget="chosen"]', context).each(function (index, item) {
            var $item = $(item);
            var config = {
                allow_single_deselect: true,
                default_text: 'Sélectionner une option',
                search_contains: true,
                no_results_text: 'Aucun résultat pour :'
            };

            $item.chosen(config);
        });

       // $('input, textarea', context).placeholder();
      // $('textarea', context).autosize();
    };

    $(document).on('bw_load', function (event) {
        initialize(event.currentTarget);
    });

    $(function () {
        initialize(document);
    });
})(window, window.jQuery, window.document);
